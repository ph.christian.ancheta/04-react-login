# Login - React App
A simple React App that allows users to login through email and passwords. Users can also logout.
For logging in, the requirement are as follows:
- email should be @ in validation
- password should be 7 or more character


## Setup

```
npm install
npm run start
```
